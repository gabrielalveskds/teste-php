# Teste Desenvolvedor PHP

Teste inicial para Vaga de Desenvolvedor PHP na DigitalHub

## Desafios

Realize os desafios que estão descritos nos seguintes arquivos: 

* bowling/README.md
* change/README.md

## Instruções

1. Faça um fork do projeto para sua conta pessoal
2. Crie uma branch por desafio
3. Adicione comentários onde achar necessário
4. Após terminar um desafio faça o merge do seu branch para o master

Obs: O teste é usado para avaliar lógica e boas práticas de programação.

Boa Sorte!
