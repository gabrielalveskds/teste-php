# Bowling

Crie uma classe que irá guardar o score de um jogo de boliche, essa classe deve ter
duas funções públicas: roll(int $numPinosDerrubados) e score().

## Pontuação

O jogo consiste de 10 frames, cada frame é composto de uma ou
duas jogadas, e 10 pinos no seu estado inicial.

Existem 3 casos base para pontuacao:
* Um frame normal ocorre quando o jogador derruba menos de 10 pinos,
  nesse caso o score do frame e igual ao numero de pinos derrubados.

* Um Spare ocorre quando todos os pinos sao derrubados na segunda
  bola jogada. Nesse caso a pontuacao total do frame sera 10 +
  o numero de pinos derrubados na proxima jogada.

* Um Strike ocorre quando todos os 10 pinos sao derrudos com a
  primeira bola jogada. O valor total desse frame sera 10 + o
  numero de pinos derrubados nas proximas duas jogadas.
  Caso um strike ocorra logo apos outro strike, entao o valor do
  strike anterior so sera determinado na proxima bola jogada.

Exemplo:

| Frame 1         | Frame 2       | Frame 3                |
| :-------------: |:-------------:| :---------------------:|
| X (strike)      | 5/ (spare)    | 9 0 (open frame)       |

Frame 1 e (10 + 5 + 5) = 20

Frame 2 e (5 + 5 + 9) = 19

Frame 3 e (9 + 0) = 9

Nesse caso o total atual e 48.

O decimo frame e um caso especial. Se ocorrer um strike o jogador tera
duas bolas extras, caso consiga um spare ele ira receber uma bola extra
( com um total maximo de 2 bolas extras - 3 bolas jogadas no frame).
Utilize o site https://boliche.com.br/esporte-boliche/contagem-dos-pontos-no-boliche/
para tirar duvidas sobre as regras do jogo.

## Requirements

O seu codigo devera ter as seguintes funções:

* "roll(pins : int)" chamado sempre que o jogador lançar uma bola.
     O argumento e o numero de pinos derrubados.
* "score() : int" sera chamado apenas no fim do jogo. Essa fução deve retornar
  o score total no jogo (apenas um numero).